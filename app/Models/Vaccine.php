<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Patient;

class Vaccine extends Model
{
    use HasFactory;
    protected $table = 'vaccines';

    public function patient(){
        return $this->hasMany(Patient::class,'vaccine_id' , 'id');
    }
}
