<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vaccine;

class Patient extends Model
{
    use HasFactory;
    protected $table = 'patients';

    public function vac(){
        return $this->belongsTo(Vaccine::class , 'vaccine_id' , 'id');
    }
}
