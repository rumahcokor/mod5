<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\Vaccine;
class PatientController extends Controller
{
    public function list(){
        $pat = Patient::all();
        $nama = 'pat';
        return view('patient', ['pat' => $pat , 'nama'=>$nama]);
    }

    public function vaccine(){
        $vac = Vaccine::all();
        $nama = 'pat';
        return view('vacinepatient', ['vac' => $vac ,'nama'=>$nama]);
    }
    public function add($id){
        $vac = Vaccine::find($id);
        $nama = 'pat';
        return view('addpatient' , ['vac'=>$vac , 'nama'=>$nama]);
    }
    public function edit($id){
        $pat = Patient::find($id);
        $nama = 'pat';
        return view('editpatient' , ['pat'=>$pat , 'nama'=>$nama]);
    }

    public function edits($id , Request $request){
        $test = Patient::find($id);
        $test->vaccine_id = $request->vacid;
        $test->name = $request->name;
        $test->nik = $request->nik;
        $test->alamat = $request->alamat;
        $test->no_hp = $request->nomer;

        if ($request->gambar !=null){
            $imageName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('ktp/'), $imageName);
            $test->image_ktp = $imageName;
        }

        $test->update();
        return redirect(route('patient'));
    }

    public function delete($id){
        $vac = Patient::find($id);
        $vac->delete();
        return redirect(route('patient'));
    }

    public function adds(Request $request){
        $test = new Patient();
        $test->vaccine_id = $request->vacid;
        $test->name = $request->name;
        $test->nik = $request->nik;
        $test->alamat = $request->alamat;
        $test->no_hp = $request->nope;

        $imageName = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('ktp/'), $imageName);
        $test->image_ktp = $imageName;
        $test->save();
        return redirect(route('patient'));
    }
}
