<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vaccine;

class VaccineController extends Controller
{
    public function list(){
        $vac = Vaccine::all();
        $nama = 'vac';
        return view('vaccine', ['vac' => $vac , 'nama'=>$nama]);
    }
    public function add(){
        $nama = 'vac';
        return view('addvacine' , ['nama'=>$nama]);
    }

    public function edit($id){
        $vac = Vaccine::find($id);
        $nama = 'vac';
        return view('editvacine' , ['vac' => $vac , 'nama'=>$nama ]);
    }

    public function delete($id){
        $vac = Vaccine::find($id);
        $vac->delete();
        return redirect(route('vaccine'));
    }
    public function edits(Request $request , $id){
        $test = Vaccine::find($id);
        $test->name = $request->name;
        $test->price = $request->price;
        $test->description = $request->description;

        if ($request->gambar != null){
            $imageName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('vaccine/'), $imageName);
            $test->image = $imageName;
        }
        $test->update();
        return redirect(route('vaccine'));
    }

    public function adds(Request $request){
        $test = new Vaccine();
        $test->name = $request->name;
        $test->price = $request->price;
        $test->description = $request->description;
        $imageName = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('vaccine/'), $imageName);
        $test->image = $imageName;
        $test->save();
        return redirect(route('vaccine'));

    }
}
