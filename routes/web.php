<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

Route::get('/home' , [\App\Http\Controllers\HomeController::class , 'home'])->name('home');
Route::get('/vaccines/' , [\App\Http\Controllers\VaccineController::class , 'list'])->name('vaccine');
Route::get('/vaccine/add' , [\App\Http\Controllers\VaccineController::class , 'add'])->name('vaccine.add');
Route::post('/vaccine/adds' , [\App\Http\Controllers\VaccineController::class , 'adds'])->name('vaccine.adds');
Route::get('/vaccine/edit/{id}' , [\App\Http\Controllers\VaccineController::class , 'edit'])->name('vaccine.edit');
Route::post('/vaccine/edit/{id}' , [\App\Http\Controllers\VaccineController::class , 'edits'])->name('vaccine.edits');
Route::get('/vaccine/delete/{id}' , [\App\Http\Controllers\VaccineController::class , 'delete'])->name('vaccine.delete');


Route::get('/patients/' , [\App\Http\Controllers\PatientController::class , 'list'])->name('patient');
Route::get('/patient/vaccine' , [\App\Http\Controllers\PatientController::class , 'vaccine'])->name('patient.vaccine');
Route::get('/patient/add/{id}' , [\App\Http\Controllers\PatientController::class , 'add'])->name('patient.add');
Route::post('/patient/add/' , [\App\Http\Controllers\PatientController::class , 'adds'])->name('patient.adds');
Route::get('/patient/edit/{id}' , [\App\Http\Controllers\PatientController::class , 'edit'])->name('patient.edit');
Route::post('/patient/edit/{id}' , [\App\Http\Controllers\PatientController::class , 'edits'])->name('patient.edits');
Route::get('/patient/delete/{id}' , [\App\Http\Controllers\PatientController::class , 'delete'])->name('patient.delete');
