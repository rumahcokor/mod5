@extends('template')

@section('content')
    <div class="container-sm">
        <div class="mt-5">
            <h1 class="text-center">About Us</h1>
            <div class="row align-item-start mt-5">
                <div class="col">
                    <img class="d-block img-fluid" src="/undraw.svg" alt="">
                </div>
                <div class="col">
                    Vaksin merupakan antigen (mikroorganisma) yang diinaktivasi atau dilemahkan yang bila diberikan kepada orang yang sehat untuk menimbulkan antibodi spesifik terhadap mikroorganisma tersebut, sehingga bila kemudian terpapar, akan kebal dan tidak terserang penyakit. Bahan dasar membuat vaksin tentu memerlukan mikroorganisma, baik virus maupun bakteri. Menumbuhkan mikroorganisma memerlukan media tumbuh yang disimpan pada suhu tertentuMikroorganisma yang tumbuh kemudian akan dipanen, diinaktivasi, dimurnikan, diformulasi dan kemudian dikemas.
                </div>

            </div>
        </div>
    </div>
@endsection
