@extends('template')
@section('content')
    <div class="container mt-5">
        <h1 class="mt-5 text-center"> Input Vaccine</h1>

        <form action="{{route('vaccine.edits' , ['id'=>$vac['id']])}}" method="post" class="container-sm" enctype="multipart/form-data">
            @csrf
            @method('post')
            <div class="mb-3">
                <label for="name" class="form-label">Vaccine Name</label>
                <input type="text" id="name" class="form-control" name="name" value="{{$vac['name']}}" placeholder="vaccine name" required>
            </div>

            <div class="mb-3">
                <label for="price" class="form-label">Price</label>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Rp</span>
                    <input type="number" id="name" class="form-control" name="price" placeholder="vaccine Price" value="{{$vac['price']}}" required>
                </div>

            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Description</label>
                <textarea type="text" id="name" class="form-control" name="description" placeholder="vaccine name" required> {{$vac['description']}}</textarea>
            </div>

            <div class="custom-file mt-3">
                <label class="custom-file-label mb-1" for="inputGroupFile01">Gambar</label> <br>
                <input type="file" class="custom-file-input" id="inputGroupFile01" name="gambar">
            </div>
            <div class="mt-3">
                <input type="submit" class="btn btn-primary" value="submit">
            </div>
        </form>

    </div>
@endsection
