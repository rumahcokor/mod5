@extends('template')

@section('content')
    @if(count($pat) == 0)
        <div class="mt-5 container">
            <p class="text-muted text-center">There Is No Data</p>
            <center><a class="text-center btn btn-sm btn-primary" href="{{route('patient.vaccine')}}">Register Patient</a></center>
        </div>
    @else
        <div class="container w-75 mt-5">
            <a href="{{route('patient.vaccine')}}" class="btn btn-primary">Add Patient</a>
            <table class="table container table-primary mt-2">
                <tr>
                    <th>#</th>
                    <th>Vaccine</th>
                    <th>Name</th>
                    <th>NIK</th>
                    <th>Alamat</th>
                    <th>No Hp</th>
                    <th>Action</th>
                </tr>
                <?php $num = 1 ?>
                @foreach($pat as $x)
                    <tr>
                        <td>{{$num}}</td>
                        <td>{{$x->vac['name']}}</td>
                        <td>{{$x['name']}}</td>
                        <td>{{$x['nik']}}</td>
                        <td>{{$x['alamat']}}</td>
                        <td>{{$x['no_hp']}}</td>
                        <td>
                            <a class="btn btn-warning" href="{{route('patient.edit' , ['id'=> $x['id']])}}">Update</a> <a class="btn btn-danger" href="{{route('patient.delete' , ['id'=>$x['id']])}}">Delete</a></td>
                    </tr>
                    <?php $num++ ?>
                @endforeach
            </table>
        </div>
    @endif
@endsection
