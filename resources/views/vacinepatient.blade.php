@extends('template')

@section('content')
    @if(count($vac) == 0)
        <div class="mt-5 container">
            <p class="text-muted text-center">There Is No Data</p>
            <center><a class="text-center btn btn-sm btn-primary" href="{{route('vaccine.add')}}">Register Vaccine</a></center>
        </div>
    @else
        <div class="container">
        <div class="row container mt-5">


                @foreach($vac as $x)

                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('vaccine/'.$x->image)}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$x['image']}}</h5>
                            <p>Rp {{$x['price']}}</p>
                            <p class="card-text fs-3">{{$x['description']}}</p>
                            <a href="{{route('patient.add' , ['id'=>$x['id']])}}" class="btn btn-primary">Vaccine Now</a>
                        </div>
                    </div>


                @endforeach

        </div>
        </div>

    @endif
@endsection
