@extends('template')
@section('content')
    <div class="container mt-5">
        <h1 class="mt-5 text-center"> Register Patient Vaccine {{$vac['name']}}</h1>

        <form action="{{route('patient.adds')}}" method="post" class="container-sm" enctype="multipart/form-data">
            @csrf
            @method('post')
            <div class="mb-3">
                <label for="name" class="form-label">Vaccine ID</label>
                <input type="text" id="vacid" class="form-control" value="{{$vac['id']}}" name="vacid" placeholder="vaccine id" required readonly>
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Patient Name</label>
                <input type="text" id="name" class="form-control" name="name" placeholder="Patient Name" required >
            </div>

            <div class="mb-3">
                <label for="price" class="form-label">NIK</label>
                <div class="input-group mb-3">
                    <input type="number" id="nik" class="form-control" name="nik" placeholder="NIK" required>
                </div>

            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Alamat</label>
                <input type="text" id="alamat" class="form-control" name="alamat" placeholder="Alamat pasien" required >
            </div>


            <div class="custom-file mt-3">
                <label class="custom-file-label mb-1" for="inputGroupFile01">KTP</label> <br>
                <input type="file" class="custom-file-input" id="inputGroupFile01" name="gambar" required>
            </div>
            <div class="mb-3 mt-3">
                <label for="price" class="form-label">Nomer Hape</label>
                <div class="input-group mb-3">
                    <input type="number" id="nope" class="form-control" name="nope" placeholder="Nomer Hape Pasien" required>
                </div>

            </div>

            <div class="mt-3">
                <input type="submit" class="btn btn-primary" value="submit">
            </div>
        </form>

    </div>
@endsection
