@extends('template')

@section('content')
    @if(count($vac) == 0)
        <div class="mt-5 container">
            <p class="text-muted text-center">There Is No Data</p>
           <center><a class="text-center btn btn-sm btn-primary" href="{{route('vaccine.add')}}">Register Vaccine</a></center>
        </div>
    @else
        <div class="container w-50 mt-5">
            <a href="{{route('vaccine.add')}}" class="btn btn-primary">Add Vacines</a>
            <table class="table container table-primary mt-2">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                <?php $num = 1 ?>
                @foreach($vac as $x)
                    <tr>
                        <td>{{$num}}</td>
                        <td>{{$x['name']}}</td>
                        <td>Rp  {{$x['price']}}</td>
                        <td>
                                <a class="btn btn-warning" href="{{route('vaccine.edit' , ['id'=> $x['id']])}}">Update</a> <a class="btn btn-danger" href="{{route('vaccine.delete' , ['id'=>$x['id']])}}">Delete</a></td>
                    </tr>
                    <?php $num++ ?>
                @endforeach
            </table>
        </div>
    @endif
@endsection
